package com.jlngls.modificadoresacessos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle


/* private --> visivel apnenas dentro da classe
protected -->F funciona como o 'private, mas é visivel em subclass
public -->  visivel em qualquer lugar, caso nao seja definido o modificador, o padrao tbm sera public
internal --> lembra o 'public , mas sua visibilidade e restriginda ao modulo, sendo entao, visivel dentro de um mesmo modulo
* */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var cao = Animal.Cao()
        println(cao.nome)





    }
}

open class Animal{

    var nome = "marley"
    fun dormir (){
        println("dormir")
    }

    class Cao : Animal(){


    }
}